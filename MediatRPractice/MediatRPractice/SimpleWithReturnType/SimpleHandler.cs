﻿namespace MediatRPractice.SimpleWithReturnType;

public class SimpleHandler : IRequestHandler<SimpleRequest, SimpleResponse>
{
    public static int Counter;

    public Task<SimpleResponse> Handle(SimpleRequest request, CancellationToken cancellationToken)
    {
        Counter++;
        return Task.FromResult(new SimpleResponse());
    }
}
