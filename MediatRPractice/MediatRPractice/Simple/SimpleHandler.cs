﻿namespace MediatRPractice.Simple;

public class SimpleHandler : IRequestHandler<SimpleRequest>
{
    public static int Counter;

    public Task Handle(SimpleRequest request, CancellationToken cancellationToken)
    {
        Counter++;  
        return Task.CompletedTask;
    }
}
