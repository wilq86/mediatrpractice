﻿namespace MediatRPractice.Behaviours;

public class Handler : IRequestHandler<Request, Response>
{
    public static int Counter;

    public Task<Response> Handle(Request request, CancellationToken cancellationToken)
    {
        Counter++;  
        return Task.FromResult(new Response());
    }
}
