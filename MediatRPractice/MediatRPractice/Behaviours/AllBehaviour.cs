﻿namespace MediatRPractice.Behaviours;

public class AllBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
{
    public static int Counter;
   

    public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
    {
        Counter++;
        return await next ();  
    }
}
