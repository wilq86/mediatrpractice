﻿namespace MediatRPractice.Behaviours;

public class Behaviour : IPipelineBehavior<Request, Response>
{
    public static int Counter;
    public static bool Enabled = true;

    public async Task<Response> Handle(Request request, RequestHandlerDelegate<Response> next, CancellationToken cancellationToken)
    {
        Counter++;
        if (Enabled)
        {
            return await next();
        }
        else
        {
            return new Response();
        }
    }
}
