﻿namespace MediatRPractice.Streaming;

public class StreamRequestHandler : IStreamRequestHandler<Request, Response>
{
    public async IAsyncEnumerable<Response> Handle(Request request, CancellationToken cancellationToken)
    {
        while(!cancellationToken.IsCancellationRequested)
        {
            await Task.Delay(500);
            yield return new Response();    
        }
    }
}
