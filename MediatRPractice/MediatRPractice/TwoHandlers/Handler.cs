﻿namespace MediatRPractice.TwoHandlers;

public class Handler : IRequestHandler<Request>
{
    public static int Counter;

    public Task Handle(Request request, CancellationToken cancellationToken)
    {
        Counter++;  
        return Task.CompletedTask;
    }
}
