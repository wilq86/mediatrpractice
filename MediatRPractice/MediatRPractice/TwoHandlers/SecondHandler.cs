﻿namespace MediatRPractice.TwoHandlers;

public class SecondHandler : IRequestHandler<Request>
{
    public static int Counter;

    public Task Handle(Request request, CancellationToken cancellationToken)
    {
        Counter++;
        return Task.CompletedTask;
    }
}
