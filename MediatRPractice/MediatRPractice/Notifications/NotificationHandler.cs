﻿namespace MediatRPractice.Notifications;

public class NotificationHandler : INotificationHandler<Notification>
{
    public static int Counter;
    public Task Handle(Notification notification, CancellationToken cancellationToken)
    {
        Counter++;
        return Task.CompletedTask;
    }
}
