﻿namespace MediatRPractice.Notifications;

public class NotificationHandlerTwo : INotificationHandler<Notification>
{
    public static int Counter;

    public Task Handle(Notification notification, CancellationToken cancellationToken)
    {
        Counter++;
        return Task.CompletedTask;
    }
}
