﻿using MediatRPractice.Streaming;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediatRPractice.Test.Streaming
{
    public class StreamingTest
    {
        [Fact]
        public async Task shouldCallStreaming()
        {
            var serviceCollection = new ServiceCollection()
                .AddMediatR(cfg => cfg.RegisterServicesFromAssemblyContaining<MediatRPracticeAssembly>())
                .BuildServiceProvider();

            var mediator = serviceCollection.GetRequiredService<IMediator>();

            var result = mediator.CreateStream(new Request());

            int count = 0;
            await foreach (var item in result)
            {
                count++;
                if (count > 2)
                {
                    break;
                }
            }
        }
    }
}
