using MediatRPractice.TwoHandlers;

namespace MediatRPractice.Test.TwoHandlers;

public class TwoHandlersTest
{
    [Fact]
    public async Task shouldCallHandler()
    {
        var serviceCollection = new ServiceCollection()
            .AddMediatR(cfg => cfg.RegisterServicesFromAssemblyContaining<MediatRPracticeAssembly>())
            .BuildServiceProvider();

        var mediator = serviceCollection.GetRequiredService<IMediator>();

        Request request = new Request();
        await mediator.Send(request);

        Handler.Counter.Should().Be(1);   
    }
}