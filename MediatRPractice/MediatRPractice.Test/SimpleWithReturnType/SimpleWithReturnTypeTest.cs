using MediatRPractice.SimpleWithReturnType;

namespace MediatRPractice.Test.SimpleWithReturnType;

public class SimpleWithReturnTypeTest
{
    [Fact]
    public async Task shouldCallHandler()
    {
        var serviceCollection = new ServiceCollection()
            .AddMediatR(cfg => cfg.RegisterServicesFromAssemblyContaining<MediatRPracticeAssembly>())
            .BuildServiceProvider();

        var mediator = serviceCollection.GetRequiredService<IMediator>();

        SimpleRequest request = new SimpleRequest();
        SimpleResponse response = await mediator.Send(request);

        SimpleHandler.Counter.Should().Be(1);   
    }
}