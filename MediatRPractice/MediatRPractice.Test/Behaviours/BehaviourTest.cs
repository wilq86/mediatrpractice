using MediatRPractice.Behaviours;

namespace MediatRPractice.Test.Behaviours;

public class BehaviourTest
{
    [Fact]
    public async Task shouldCallBehavious()
    {
        var serviceCollection = new ServiceCollection()
            .AddMediatR(cfg => cfg.RegisterServicesFromAssemblyContaining<MediatRPracticeAssembly>())
            .AddTransient(typeof(IPipelineBehavior<,>), typeof(AllBehaviour<,>))
            .AddTransient(typeof(IPipelineBehavior<Request, Response>), typeof(Behaviour))
            .BuildServiceProvider();

        var mediator = serviceCollection.GetRequiredService<IMediator>();

        {
            var resposne = await mediator.Send(new Request());

            AllBehaviour<Request, Response>.Counter.Should().Be(1);
            Behaviour.Counter.Should().Be(1);
            Handler.Counter.Should().Be(1);
        }

        {
            Behaviour.Enabled = false;

            var resposne = await mediator.Send(new Request());
            AllBehaviour<Request, Response>.Counter.Should().Be(2);
            Behaviour.Counter.Should().Be(2);
            Handler.Counter.Should().Be(1);
        }
    }
}