global using FluentAssertions;
global using MediatR;
global using Microsoft.Extensions.DependencyInjection;
global using Xunit;
