using MediatRPractice.Simple;

namespace MediatRPractice.Test.Simple;

public class SimpleTest
{
    [Fact]
    public async Task shouldCallHandler()
    {
        var serviceCollection = new ServiceCollection()
            .AddMediatR(cfg => cfg.RegisterServicesFromAssemblyContaining<MediatRPracticeAssembly>())
            .BuildServiceProvider();

        var mediator = serviceCollection.GetRequiredService<IMediator>();

        SimpleRequest request = new SimpleRequest();
        await mediator.Send(request);

        SimpleHandler.Counter.Should().Be(1);   
    }
}