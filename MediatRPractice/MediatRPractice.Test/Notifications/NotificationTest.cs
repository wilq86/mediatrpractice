﻿using MediatRPractice.Notifications;

namespace MediatRPractice.Test.Notifications;

public class NotificationTest
{
    [Fact]
    public async Task shouldCallNotification()
    {
        var serviceCollection = new ServiceCollection()
            .AddMediatR(cfg => cfg.RegisterServicesFromAssemblyContaining<MediatRPracticeAssembly>())
            .BuildServiceProvider();

        var mediator = serviceCollection.GetRequiredService<IMediator>();

        
        await mediator.Publish(new Notification());

        NotificationHandler.Counter.Should().Be(1);
        NotificationHandlerTwo.Counter.Should().Be(1);
    }
}
